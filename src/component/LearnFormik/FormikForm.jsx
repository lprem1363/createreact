import {  Form, Formik,  } from 'formik'
import React from 'react'
import * as yup from "yup"
import FormikInput from './FormikInput.jsx'
import FormikTextArea from './FormikTextArea.jsx'
import FormikSelect from './FormikSelect.jsx'
import FormikRadio from './FormikRadio.jsx'
let initialValues={
    firstName:"",
    lastName:"",
    description:"",
}

let onSubmit=(value,other)=>{
    console.log(value)
}

let validateSchema=yup.object({
    firstName:yup.string().required("First Name is required"),
    lastName:yup.string().required("Last Name is required"),
    email:yup.string().required("email is required"),
    password:yup.string().required("password is required"),
    country:yup.string().required("country name is required"),
    gender:yup.string().required("Gender is required"),
    description:yup.string()
})

let countryOptions=[
    {
        label:"select country",
        value:"",
        disabled:true
    },
    {
        label:"Nepal",
        value:"Nepal",
    },
    {
        label:"China",
        value:"China",
    },
    {
        label:"India",
        value:"India",
    },
]

let genderOptions=[
    {
        label:"Male",
        value:"male",
       
    },
    {
        label:"Female",
        value:"female",
    },
    {
        label:"Other",
        value:"other",
    },
   
]

const FormikForm = () => {
  return (
    <div>
    <Formik
    initialValues={initialValues}
    onSubmit={onSubmit}
    validationSchema={validateSchema}
    >
    {
        (formik)=>{
            return(
                <Form>
                   
                    <FormikInput 
                    name="firstName" 
                    label="First Name" 
                    type="text" 
                    onChange={(e)=>{
                     formik.setFieldValue("firstName",e.target.value) }}
                     placeholder="firstName"
                     required={true}
                    //  style={{backgroundcolor: "red" }}
                     >


                     </FormikInput>

                    <FormikInput 
                    name="lastName" 
                    label="Last Name" 
                    type="text"
                    onChange={(e)=>{
                        formik.setFieldValue("lastName",e.target.value)}}
                        placeholder="lastName"
                        required={true}

                       >
                         </FormikInput>
                

                     <FormikTextArea 
                    name="description" 
                    label="Description" 
                    type="text"
                    onChange={(e)=>{
                        formik.setFieldValue("description",e.target.value)}}
                        placeholder="description"
                        required={false}

                       >
                         </FormikTextArea>

                     <FormikSelect 
                    name="country" 
                    label="Country" 
                    onChange={(e)=>{
                        formik.setFieldValue("country",e.target.value)}}
                        required={true}
                        options={countryOptions}

                       >
                         </FormikSelect>
                         
                     <FormikRadio 
                    name="gender" 
                    label="Gender" 
                    onChange={(e)=>{
                        formik.setFieldValue("gender" ,e.target.value)}}
                        required={true}
                        options={genderOptions}

                       >
                         </FormikRadio>
                
                         <FormikInput 
                    name="email" 
                    label="email" 
                    type="email" 
                    onChange={(e)=>{
                     formik.setFieldValue("email",e.target.value) }}
                     placeholder="email"
                     required={true}
                    //  style={{backgroundcolor: "red" }}
                     >


                     </FormikInput>

                     <FormikInput 
                    name="password" 
                    label="Password" 
                    type="password" 
                    onChange={(e)=>{
                     formik.setFieldValue("password",e.target.value) }}
                     placeholder="password"
                     required={true}
                    //  style={{backgroundcolor: "red" }}
                     >


                     </FormikInput>

                    <button type="submit">submit</button>
                </Form>
            )
        }}
    </Formik>
    </div>
  )
}

export default FormikForm