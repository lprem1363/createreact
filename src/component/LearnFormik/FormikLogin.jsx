import { Form, Formik } from 'formik'
import React from 'react'
import * as yup from "yup"
import FormikInput from './FormikInput.jsx'
import { NavLink } from 'react-router-dom'


const FormikLogin
 = () => {
    let initialValues={
        firstName:"",
        lastName:"",
        description:"",
        country:"",
        gender:"", 
    }

    let onSubmit=(value,other)=>{
        console.log(value)
    }

    let validateSchema=yup.object({

        email:yup.string().required("email is required"),
        password:yup.string().required("password is required"),

    })

    // let countryOptions=[
    //     {
    //         label:"select country",
    //         value:"",
    //         disabled:true
    //     },
    //     {
    //         label:"Nepal",
    //         value:"Nepal",
    //     },
    //     {
    //         label:"China",
    //         value:"China",
    //     },
    //     {
    //         label:"India",
    //         value:"India",
    //     },
    // ]
    
    // let genderOptions=[
    //     {
    //         label:"Male",
    //         value:"male",
           
    //     },
    //     {
    //         label:"Female",
    //         value:"female",
    //     },
    //     {
    //         label:"Other",
    //         value:"other",
    //     },
       
    // ]
  return (
    <div>
         <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
      <div className="sm:mx-auto sm:w-full sm:max-w-sm">
        {/* <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
          Login
        </h2> */}
      </div>
      <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
    <Formik
    initialValues={initialValues}
    onSubmit={onSubmit}
    validationSchema={validateSchema}
    >
    {
        (formik)=>{
            return(
                <Form className="space-y-6">   

                     <FormikInput 
                    name="email" 
                    label="email" 
                    type="email" 
                    onChange={(e)=>{
                     formik.setFieldValue("email",e.target.value) }}
                     placeholder="email"
                     required={true}
                    //  style={{backgroundcolor: "red" }}
                     >

                     </FormikInput>

                     <FormikInput 
                    name="password" 
                    label="Password" 
                    type="password" 
                    onChange={(e)=>{
                     formik.setFieldValue("password",e.target.value) }}
                     placeholder="password"
                     required={true}
                    //  style={{backgroundcolor: "red" }}
                     >

                     </FormikInput>
                     <button type="submit"className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
>Login</button>
                </Form>
            )
        }}
    </Formik>
    <p className="mt-10 text-center text-sm text-gray-500">
          Don't have an account yet?{" "}
          <NavLink
            to="/signup"
            className="font-semibold leading-6 text-indigo-600 hover:text-indigo-500"
          >
            Sign Up
          </NavLink>
        </p>
    </div>
    </div>
    </div>
  )
}

export default FormikLogin
