import { Form, Formik } from 'formik'
import React from 'react'
import * as yup from "yup"
import FormikInput from './FormikInput'
import { useNavigate } from 'react-router-dom'

const FormikSignUp = () => {
    let initialValues={
        firstName:"",
        lastName:"",
        email:"",
        password:"",
        // description:"",
        // country:"Nepal",
        // gender:"Male", 
    }

    let onSubmit=(value,other)=>{
        console.log(value)
    }

    let validateSchema=yup.object({
        firstName:yup.string().required("First Name is required")
        .min(4,"must be atleast 4 characters").max(10,"must be atmost 10 characters")
        .matches(/^[a-zA-Z ]*$/,"only alphabet and space"),
        lastName:yup.string().required("Last Name is required"),
        email:yup.string().required("email is required"),
        password:yup.string().required("password is required"),
        // country:yup.string().required("country name is required"),
        // gender:yup.string().required("Gender is required"),
        // description:yup.string()
    })
    let navigate=useNavigate()

    // let countryOptions=[
    //     {
    //         label:"select country",
    //         value:"",
    //         disabled:true
    //     },
    //     {
    //         label:"Nepal",
    //         value:"Nepal",
    //     },
    //     {
    //         label:"China",
    //         value:"China",
    //     },
    //     {
    //         label:"India",
    //         value:"India",
    //     },
    // ]
    
    // let genderOptions=[
    //     {
    //         label:"Male",
    //         value:"Male",
           
    //     },
    //     {
    //         label:"Female",
    //         value:"Female",
    //     },
    //     {
    //         label:"Other",
    //         value:"Other",
    //     },
       
    // ]
  return (
    <div>
         <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
      <div className="sm:mx-auto sm:w-full sm:max-w-sm">
        <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
          Sign up
        </h2>
      </div>
      <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
    <Formik
    initialValues={initialValues}
    onSubmit={onSubmit}
    validationSchema={validateSchema}
    >
    {
        (formik)=>{
            return(

                <Form className="space-y-6">   
                     
                     <FormikInput 
                    name="firstName" 
                    label="First Name" 
                    type="text" 
                    onChange={(e)=>{
                     formik.setFieldValue("firstName",e.target.value) }}
                     placeholder="firstName"
                     required={true}
                     >
                    </FormikInput>

                    <FormikInput 
                    name="lastName" 
                    label="Last Name" 
                    type="text"
                    onChange={(e)=>{
                    formik.setFieldValue("lastName",e.target.value)}}
                     placeholder="lastName"
                    required={true} >
                     </FormikInput>

                     <FormikInput 
                    name="email" 
                    label="email" 
                    type="email" 
                    onChange={(e)=>{
                     formik.setFieldValue("email",e.target.value) }}
                     placeholder="email"
                     required={true}
                     >

                     </FormikInput>

                     <FormikInput 
                    name="password" 
                    label="Password" 
                    type="password" 
                    onChange={(e)=>{
                     formik.setFieldValue("password",e.target.value) }}
                     placeholder="password"
                     required={true}
                    //  style={{backgroundcolor: "red" }}
                     >

                     </FormikInput>

                    
{/* 
                     <FormikSelect 
                    name="country" 
                    label="Country" 
                    onChange={(e)=>{
                        formik.setFieldValue("country",e.target.value)}}
                        required={true}
                        options={countryOptions}

                       >
                         </FormikSelect>
                         
                     <FormikRadio 
                    name="gender" 
                    label="Gender" 
                    onChange={(e)=>{
                        formik.setFieldValue("gender" ,e.target.value)}}
                        required={true}
                        options={genderOptions}

                       >
                         </FormikRadio>
                
                         <FormikTextArea 
                    name="description" 
                    label="Description" 
                    type="text"
                    onChange={(e)=>{
                        formik.setFieldValue("description",e.target.value)}}
                        placeholder="description"
                        required={false}

                       >
                         </FormikTextArea> */}

                     <button type="submit" 
                     onclick={()=>{
                      navigate('/login')
                     }}
                     className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
>signin</button>
                     
                </Form>
            )
        }}
    </Formik>
    </div>
    </div>
    </div>
  )
}

export default FormikSignUp