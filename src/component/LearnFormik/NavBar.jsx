import React from 'react'
import { NavLink, Outlet, Route, Routes } from 'react-router-dom'
import Board from './Board'
import Temperature from './Temperature'
import Humidity from './Humidity'
import FormikLogin from './FormikLogin'
import FormikSignUp from './FormikSignUp'

const NavBar = () => {
  return (
    <div>
          <h1  className="font-semibold leading-6 text-indigo-600 hover:text-indigo-500" >WELCOME TO AUTOMATIC GREENHOUSE</h1>
        
        {/* <LoginUser></LoginUser> */}
                
                
        {/*            
                  <Routes>
                    <Route path="/" element={<Outlet></Outlet>}></Route>
                    <Route index element={<FormikLogin></FormikLogin>}></Route>
                    <Route path="signup" element={<FormikSignUp></FormikSignUp>}></Route>
                  </Routes> */}
                  <nav style={{backgroundColor:"lightblue"}}>
                  <NavLink
            to="/temperature" style={{marginLeft:"80px", color:"white" }}>
            Temperature
          </NavLink>
                  <NavLink
            to="/humidity" style={{marginLeft:"20px", color:"white" }}>
            Humidity 
          </NavLink>
                  <NavLink
            to="/soilMoisture" style={{marginLeft:"20px", color:"white" }}>
            Soil Moisture
          </NavLink>
                  <NavLink
            to="/login" style={{marginLeft:"20px", color:"white" }}>
            LoginUser
          </NavLink>
          </nav>
                  <Routes>
                    <Route path="/" element={<Outlet></Outlet>}></Route>
                    <Route index element={<Board></Board>}></Route>
                    <Route path="temperature" element={<Temperature></Temperature>}></Route>
                    <Route path="humidity" element={<Humidity></Humidity>}></Route>
                    <Route path="soilMoisture" element={<soilMoisture></soilMoisture>}></Route>
                    <Route path="login" element={<FormikLogin></FormikLogin>}></Route>
                    <Route path="signup" element={<FormikSignUp></FormikSignUp>}></Route>
                        {/* <Route index element={<FormikSignUp></FormikSignUp>}></Route>
                        <Route path="login" element={<FormikLogin></FormikLogin>}></Route> */}
        
        
                  </Routes>
                  
    </div>
  )
}

export default NavBar